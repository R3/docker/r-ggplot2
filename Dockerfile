FROM rocker/r-ver

MAINTAINER R3 team <lcsb-r3@uni.lu>

RUN echo 'install.packages(c("httr", "jsonlite", "devtools", "ggplot2", "plyr", "reshape2", "RColorBrewer", "scales", "FactoMineR", \
"Hmisc", "cowplot", "shiny", "sitools"), repos="http://cran.us.r-project.org", dependencies=TRUE)' > /tmp/packages.R \
&& Rscript /tmp/packages.R
